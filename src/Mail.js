import React, {useState} from "react";
import "./Mail.css"
import {IconButton} from "@material-ui/core";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import MoveToInboxIcon from '@material-ui/icons/MoveToInbox';
import ErrorIcon from '@material-ui/icons/Error';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeleteIcon from '@material-ui/icons/Delete';
import UnfoldMoreIcon from '@material-ui/icons/UnfoldMore';
import PrintIcon from '@material-ui/icons/Print';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {useHistory} from "react-router-dom";
import LabelImportantIcon from "@material-ui/icons/LabelImportant";
import {useSelector} from "react-redux";
import{selectOpenMail} from "./features/mailSlice"
import firebase, {db} from "./firebase"
function Mail(){
    const history=useHistory();
    const selectedMail=useSelector(selectOpenMail);
    const [emails,setEmails]=useState([]);

const handleDelete=(id)=>{
    db.collection('emails').orderBy('timestamp',"desc").onSnapshot(snapshot => setEmails(snapshot.docs.map(doc=>({id:doc.id,
        data :doc.data(),}))))
    }
return (
    <div className="mail" >
        <div className="mail_tools">
            <div className="mail_toolsLeft">
                <IconButton onClick={()=>history.push("/")}>
                    <ArrowBackIcon/>
                </IconButton>



            </div>
            <div className="mail-toolsRight">



                <IconButton onClick={()=>history.push("/")}>
                <ExitToAppIcon/>
            </IconButton>
            </div>
        </div>
        <div className="mail_body">
            <div className="mail_bodyHeader">

                <h2>{selectedMail?.subject}</h2>
                <LabelImportantIcon className="mail_important"/>
                <p>{selectedMail?.title}</p>
                <p className="mail_time">{selectedMail?.time}</p>
            </div>
            <div className="mail_message">
                <p>{selectedMail?.description}</p>
            </div>
        </div>
    </div>
)
}
export default  Mail;
import React from "react";
import "./SendMail.css";
import CloseIcon from"@material-ui/icons/Close";
import {Button} from "@material-ui/core";
import {useForm} from"react-hook-form"
import {closeSendMessage} from "./features/mailSlice";
import {useDispatch} from "react-redux";
import{db} from "./firebase";
import firebase from "firebase";
import emailjs from "emailjs-com"



function SendMail(){
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const dispatch=useDispatch();
const onSubmit =(formdata) =>{
    console.log(formdata);
    db.collection('emails').add(
        {
        To:formdata.To,
            Subject: formdata.Subject,
            Message:formdata.Message,
            timestamp: firebase.firestore.FieldValue.serverTimestamp()

    }

    );

    emailjs.send('gmail-chadha', 'template_4nx8w6l', {
        Subject: formdata.Subject,
        To: formdata.To,
        name: "waouma97@gmail.com",
        Message: formdata.Message,}, 'user_lSJRdzV8gxoqUoLBc7uup')
        .then((result) => {
            console.log(result.text);
        }, (error) => {
            console.log(error.text);
        });
    dispatch(closeSendMessage());
};

    return (
        <div className="sendMail">
            <div className="sendMail_header">
                <h3>New Message</h3>
                <CloseIcon  onClick={()=> dispatch(closeSendMessage())} className="sendMail_close"/>
            </div>
             <form onSubmit={handleSubmit(onSubmit)}>
                 <input {...register("To", {required: true})}  placeholder='To' type="email"
                 />
                 {errors.To && <p className="sendMail_error">To is required </p> }
                 <input {...register("Subject", {required: true})} placeholder='Subject' type="text" />
                 {errors.Subject && <p className="sendMail_error">Subject is required </p> }

                 <input  {...register("Message", {required: true})}placeholder='Message...' type="text" className="sendMail_message" />
                 {errors.Message && <p className="sendMail_error">Message is required </p> }

                 <div className="sendMail_options">
                <Button className="sendMail_send" variant="contained" color="primary" type="submit">Send</Button></div>
             </form>
        </div>
    )
}

export default SendMail;
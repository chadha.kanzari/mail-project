 import React, {useEffect, useState} from "react";
import "./Emaillist.css";
import {Checkbox,IconButton} from "@material-ui/core";
 import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
 import RedoIcon from '@material-ui/icons/Redo';
 import MoreVertIcon from '@material-ui/icons/MoreVert';
 import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
 import ChevronRightIcon from '@material-ui/icons/ChevronRight';
 import KeyboardHideIcon from '@material-ui/icons/KeyboardHide';
 import InboxIcon from "@material-ui/icons/Inbox";
 import Section from "./Section";
 import PeopleIcon from '@material-ui/icons/People';
 import LocalOfferIcon from '@material-ui/icons/LocalOffer';
 import EmailRow from "./EmailRows";
 import firebase, {db} from "./firebase";
 import DeleteIcon from "@material-ui/icons/Delete";
 function Emaillist(){
     const [emails,setEmails]=useState([]);
     useEffect(()=>{
         db.collection('emails').orderBy('timestamp',"desc").onSnapshot(snapshot => setEmails(snapshot.docs.map(doc=>({id:doc.id,
         data :doc.data(),}))))
     })

    return(
        <div className='emaillist'>
            <div className="emaillist_settings">
                <div className="emaillist_settingsLeft">


                </div>
                <div className="emaillist_settingsRight">



                </div>
            </div>
<div className="emaillist_sections">
    <Section Icon={InboxIcon} title="Primary" color="red" selected />


</div>
            <div className="emailList_list">

                {emails.map(({id,data :{To,Subject,Message,timestamp}})=>(
                    <EmailRow
                    id={id}
                    key={id}
                    title={To}
                    subject={Subject}
                    description={Message}
                    time={new Date(timestamp?.seconds*1000).toUTCString()}
                    />



                    ))}



</div>
        </div>
    )
 }
 export default Emaillist ;
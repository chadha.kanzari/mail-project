import React from "react";
import "./Sidebar.css"
import {Button, IconButton} from "@material-ui/core";
import AddIcon from  "@material-ui/icons/Add";
import InboxIcon from '@material-ui/icons/Inbox';
import StarIcon from '@material-ui/icons/Star';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import NearMeIcon from '@material-ui/icons/NearMe';
import NoteIcon from '@material-ui/icons/Note';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PersonIcon from '@material-ui/icons/Person';
import DuoIcon from '@material-ui/icons/Duo';
import SidebarOption from "./SidebarOption";
import PhoneIcon from '@material-ui/icons/Phone';
import {useDispatch} from "react-redux";
import {openSendMessage} from "./features/mailSlice";
 function Sidebar (){

     const dispatch=useDispatch();

     return (
         <div className="sidebar">

             <Button
                 startIcon={<AddIcon fontSize="large"/>}
                 className="sidebar_compose" onClick={()=>dispatch(openSendMessage())}
                     > Compose
             </Button>







         </div>




     );

 }
 export default Sidebar ;
import React from "react";
import "./Login.css"
import {Button} from "@material-ui/core";
import {auth, provider} from "./firebase"
import {login} from "./features/userSlice";
import {useDispatch} from "react-redux";

function Login(){
    const dispatch=useDispatch();
    const signIn=()=>{
        auth.signInWithPopup(provider)
            .then(({user})=>{
                dispatch(
                    login({
                        displayName:user.displayName,
                        email:user.email,
                        photoUrl:user.photoURL,
                    })
                )
            })
            .catch((error) => alert(error.message));
    }



    return(

        <div className="login">
            <div className="login_container">
                <img
        src="message.png" alt=""

                /> <br></br>
                <Button variant="contained" color="primary" onClick={signIn}>Login</Button><br></br>
                <Button variant="contained" color="primary" href="http://localhost:3000">back to dashboard</Button>

            </div>



        </div>
    );
}
export default Login
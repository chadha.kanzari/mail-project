import React, {useState} from "react";
import "./Header.css";
import MenuIcon from '@material-ui/icons/Menu';
import {Avatar,IconButton} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';

import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import AppsIcon from '@material-ui/icons/Apps';
import NotificationsIcon from '@material-ui/icons/Notifications';
import {useDispatch, useSelector} from "react-redux";
import {logout, selectUser} from "./features/userSlice";
import {auth} from"./firebase"
import SettingsIcon from "@material-ui/icons/Settings";
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
function Header(){
    const user=useSelector((selectUser));
    const dispatch=useDispatch();
    const [emails,setEmails]=useState([]);
const signOut=()=>{
    auth.signOut().then(()=>{
      dispatch(logout())
    })
    }
    return (
       <div className="header">
<div className="header_left">
           <div className="header_left"><IconButton><MeetingRoomIcon onClick={signOut} href="http://localhost:3000"></MeetingRoomIcon></IconButton>
           <img src="messageb.png" alt=""/>
           </div>

           </div>
           <div className="header_middle">
               <SearchIcon/>
               <input  placeholder="search mail" type="text" />
               <ArrowDropDownIcon className="header_inputCaret"/>


           </div>
           <div className="header_right">


               <Avatar onClick={signOut} src={user?.photoUrl}/>







           </div>


       </div>
    )
}

export default  Header ;